
DROP DATABASE `custumer`;
CREATE DATABASE Customer;
Use Customer;
CREATE TABLE `Customer` (
  `CID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  `SURNAME` varchar(50) NOT NULL,
  `NATIO` varchar(3) NOT NULL,
  `GENDER` varchar(1) NOT NULL,
  `PHONE` varchar(9) NOT NULL,
  `DOB` date NOT NULL,
  PRIMARY KEY (`CID`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
CREATE TABLE `Account` (
  `AID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ATYPE` enum('PER','ASSOC','ENTER') DEFAULT 'PER',
  `BAL` decimal(12,2) DEFAULT '0.00',
  `DESCRI` text NOT NULL,
  `ODATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`AID`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

CREATE TABLE `Autentication` (
  `CID` bigint(20) NOT NULL,
  `Pass` varchar(200) NOT NULL,
  `attemps` int(11) NOT NULL DEFAULT '0',
role varchar(20) not null default 'ROLE_Customer',
  PRIMARY KEY (`CID`),
  CONSTRAINT `autentication_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `Customer` (`CID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Notes` (
  `NID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TEX` text NOT NULL,
  `DATECREATION` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CID` bigint(20) NOT NULL,
  PRIMARY KEY (`NID`),
  KEY `CID` (`CID`),
  CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `Customer` (`CID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Relac` (
  `CID` bigint(20) NOT NULL,
  `AID` bigint(20) NOT NULL,
  PRIMARY KEY (`CID`,`AID`),
  KEY `AID` (`AID`),
  CONSTRAINT `relac_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `Customer` (`CID`) ON DELETE CASCADE,
  CONSTRAINT `relac_ibfk_2` FOREIGN KEY (`AID`) REFERENCES `Account` (`AID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Transact` (
  `TID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACIDO` bigint(20) NOT NULL,
  `ACIDD` bigint(20) NOT NULL,
  `TAMOUNT` decimal(12,2) NOT NULL,
  `TDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`TID`),
  KEY `ACIDO` (`ACIDO`),
  KEY `ACIDD` (`ACIDD`),
  CONSTRAINT `transact_ibfk_1` FOREIGN KEY (`ACIDO`) REFERENCES `Account` (`AID`) ON DELETE CASCADE,
  CONSTRAINT `transact_ibfk_2` FOREIGN KEY (`ACIDD`) REFERENCES `Account` (`AID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



##verify the diference between an amount and a account balance
DROP FUNCTION IF EXISTS AMOUNTDIFF;
delimiter |
CREATE function AMOUNTDIFF(AMOUNT DECIMAL(12,2), AID bigint) 
RETURNS DECIMAL(12,2)
	BEGIN
		DECLARE balance decimal(12,2);
		SELECT Account.BAL INTO balance FROM Account WHERE Account.AID = AID;
		RETURN (balance-AMOUNT);
	END;
|delimiter ;
;
#Verifies is an account exists
delimiter |
CREATE function VerifyAccount(AIDV bigint) 
RETURNS BIGINT
	BEGIN
		DECLARE AUX BIGINT;
		SELECT COUNT(Account.AID) INTO AUX FROM Account WHERE Account.AID = AIDV;
		RETURN AUX;
	END;
|delimiter ;
#Verifies is an account exists
delimiter |
CREATE function VerifyCustomer(CIDV bigint) 
RETURNS BIGINT
	BEGIN
		DECLARE AUX BIGINT;
		SELECT COUNT(customer.CID) INTO AUX FROM Customer WHERE Customer.CID = CIDV;
		RETURN AUX;
	END;
|delimiter ;
delimiter |
CREATE function VerifyNotes(NIDV bigint) 
RETURNS BIGINT
	BEGIN
		DECLARE AUX BIGINT;
		SELECT COUNT(Notes.NID) INTO AUX FROM Notes WHERE Notes.NID = NIDV;
		RETURN AUX;
	END;
|delimiter ;
#Crete a transaction an verifies 
delimiter |
CREATE PROCEDURE DOTRANSACTION(AMNT decimal(12,2), AIDD bigint,AIDO bigint) 
	BEGIN
		Declare	OUTPUT int;
		DECLARE  balanceDIff decimal(12,2);
		Declare VERIFYIDD,VERIFYIDO bigint;
		SET VERIFYIDD= VerifyAccount(AIDD);
		SET VERIFYIDO= VerifyAccount(AIDO);		
		SET balanceDiff =AMOUNTDIFF(AMNT,AIDO);
			SET OUTPUT=-5;
			IF AIDD=AIDO THEN
			SET OUTPUT =-2;
			ELSEIF  VERIFYIDD!=1 THEN
				SET OUTPUT=-3;
			ELSEIF VERIFYIDO!=1 THEN
				SET OUTPUT=-4;
			ELSEIF balanceDIFF<0 THEN 
				SET OUTPUT=-1;
			ELSE
				START TRANSACTION;
						INSERT INTO Transact (ACIDO,ACIDD,TAMOUNT) VALUES (AIDO,AIDD,AMNT);
						UPDATE Account SET BAL=BAL-AMNT WHERE Account.AID = AIDO;
						UPDATE Account SET BAL=BAL+AMNT WHERE Account.AID = AIDD;
						SET OUTPUT=1;
						COMMIT;
			END IF;
select OUTPUT;
	END;
|delimiter ;
DELIMITER $$
CREATE FUNCTION `doLogin`(CIDV bigint, pass varchar(200)) RETURNS Int
BEGIN
		DECLARE AUX BIGINT;
		SELECT COUNT(Autentication.CID) INTO AUX FROM Autentication WHERE Autentication.CID = CIDV and Autentication.Pass=pass;
		RETURN AUX;
	END$$
DELIMITER ;

#update account
#	returns -5 general error
#			-4 row with key having value of id does not exists
#			1 Transaction succes
delimiter |
CREATE PROCEDURE UpdateAccount(typ text,descr text,id bigint) 
	BEGIN
		Declare OUTPUT int;	
		Declare VERIFYID bigint;
		SET VERIFYID= VerifyAccount(id);
			SET OUTPUT=-5;
			IF VERIFYID!=1 THEN
				SET OUTPUT=-4;
			ELSE
				START TRANSACTION;
						UPDATE Account SET Account.ATYPE=typ, Account.DESCRI=descr where Account.AID=id;
						SET OUTPUT=1;
						COMMIT;
			END IF;
	SELECT OUTPUT;
	END;

|delimiter ;
delimiter |
CREATE PROCEDURE UpdateNotes(te text,id bigint) 
	BEGIN
		Declare OUTPUT int;	
		Declare VERIFYID bigint;
		SET VERIFYID= VerifyNotes(id);
			SET OUTPUT=-5;
			IF VERIFYID!=1 THEN
				SET OUTPUT=-4;
			ELSE
				START TRANSACTION;
						UPDATE Notes SET Notes.TEX=te where Notes.NID=id;
						SET OUTPUT=1;
						COMMIT;
			END IF;
	SELECT OUTPUT;
	END;

|delimiter ;
#update customer
#	returns -5 general error
#			-4 row with key having value of id does not exists
#			1 Transaction succes
delimiter |
CREATE PROCEDURE UpdateCustomer(nam text,surnam text,nati varchar(3), gend varchar(1), phon varchar(9),dob timestamp,id bigint) 
	BEGIN
		Declare VERIFYID bigint;
		Declare OUTPUT int;
		SET VERIFYID= VerifyCustomer(id);
			SET OUTPUT=-5;
			IF VERIFYID!=1 THEN
				SET OUTPUT=-4;
			ELSE
				START TRANSACTION;
						UPDATE Customer SET Customer.NAME=nam, customer.SURNAME=surnam, customer.NATIO=nati, customer.GENDER=gend, customer.DOB=dob, customer.PHONE=phon where Customer.CID=id;
						SET OUTPUT=1;
						COMMIT;
			END IF;
	SELECT OUTPUT;
	END;

|delimiter ;
#update Note
#	returns -5 general error
#			-4 row with key having value of id does not exists
#			1 Transaction succes
delimiter |
CREATE PROCEDURE UpdateNote(te text, id bigint) 
	BEGIN
		Declare OUTPUT int;
		Declare VERIFYID bigint;
		SET VERIFYID= VerifyAccount(id);
			SET OUTPUT=-5;
			IF VERIFYID!=1 THEN
				SET OUTPUT=-4;
			ELSE
				START TRANSACTION;
						UPDATE Notes SET Notes.TEX=te where Notes.NID=id;
						SET OUTPUT=1;
						COMMIT;
			END IF;
	SELECT OUTPUT;
	END;

|delimiter ;
delimiter |
CREATE PROCEDURE AggregateCustomerAccount(c bigint, a bigint) 
	BEGIN
		Declare OUTPUT int;
		Declare VERIFYIDA,VERIFYIDC,VerifyKeys bigint;
		SET VERIFYIDA= VerifyAccount(a);
		SET VERIFYIDC= VerifyCustomer(c);
		SELECT count(*) INTO Verifykeys from relac where relac.AID=a and relac.cid=c;
			SET OUTPUT=-5;
			IF VerifyKeys !=0 THEN
				SET OUTPUT=-2;
			ELSEIF VERIFYIDA!=1 THEN
				SET OUTPUT=-4;
			ELSEIF VERIFYIDC!=1 THEN
				SET OUTPUT=-3;
			ELSE
				START TRANSACTION;
						INSERT INTO Relac (Relac.AID,Relac.CID) values (a,c);
						SET OUTPUT=1;
						COMMIT;
			END IF;
	SELECT OUTPUT;
	END;
|delimiter ;
delimiter |
#prevent force the id and force the autpoincrement on insert;
CREATE TRIGGER NULL_ID_Customer BEFORE INSERT ON Customer
  FOR EACH ROW
  BEGIN
	SET New.CID=NULL;

  END;
|

delimiter ;
delimiter |
CREATE TRIGGER customer_autentication After INSERT ON Customer
  FOR EACH ROW
  BEGIN
	Insert into Autentication (CID,Pass) values (new.CID,SHA1(12345));
  END;
|
delimiter ;
delimiter |
CREATE TRIGGER NULL_VALUES_Transact BEFORE INSERT ON Transact
  FOR EACH ROW
  BEGIN
	SET New.TID=NULL;

  END;
|
delimiter ;
delimiter |
CREATE TRIGGER NULL_VALUES_Notes BEFORE INSERT ON Notes
  FOR EACH ROW
  BEGIN
	SET New.NID=NULL;

	SET NEW.DATECREATION=NULL;
  END;
|
delimiter ;
delimiter |
CREATE TRIGGER NULL_VALUES_Account BEFORE INSERT ON Account
  FOR EACH ROW
  BEGIN
	SET New.AID=NULL;
SET New.ODATE=NULL;
  END;
|
delimiter ;


delimiter ;
#dump customer to system
LOCK TABLES Customer WRITE;

INSERT INTO Customer VALUES (1,'NameCustomer2','SurnameCustomer2','pt','M','911111112','1990-05-13'),(2,'NameCustomer2','SurnameCustomer2','pt','M','911111112','1990-05-13'),(3,'NameCustomer3','SurnameCustomer3','pt','M','911111112','1990-05-14'),(4,'NameCustomer4','SurnameCustomer4','pt','M','911111112','1990-05-15'),(5,'NameCustomer5','SurnameCustomer5','pt','M','911111112','1990-05-16'),(6,'NameCustomer6','SurnameCustomer6','pt','M','911111112','1990-05-17'),(7,'NameCustomer7','SurnameCustomer7','pt','M','911111112','1990-05-18'),(8,'NameCustomer8','SurnameCustomer8','pt','M','911111112','1990-05-19'),(9,'NameCustomer9','SurnameCustomer9','pt','M','911111112','1990-05-18'),(10,'NameCustomer10','SurnameCustomer10','pt','M','911111112','1990-05-28'),(11,'NameCustomer21','SurnameCustomer21','pt','M','911111112','1990-05-13'),(12,'NameCustomer23','SurnameCustomer23','pt','M','911111112','1990-05-12'),(13,'NameCustomer24','SurnameCustomer24','pt','M','911111112','1990-05-01'),(14,'NameCustomer25','SurnameCustomer25','pt','M','911111112','1991-05-13'),(15,'NameCustomer26','SurnameCustomer26','pt','M','911111112','1990-05-13'),(16,'NameCustomer27','SurnameCustomer27','pt','M','911111112','2000-05-13'),(17,'NameCustomer28','SurnameCustomer28','pt','M','911111112','1990-05-13'),(18,'NameCustomer29','SurnameCustomer29','pt','M','911111112','1990-05-13'),(19,'NameCustomer211','SurnameCustomer211','pt','M','911111112','1990-05-13'),(20,'NameCustomer212','SurnameCustomer212','pt','M','911111112','1990-05-13');

unlock Tables;
LOCK TABLES Account WRITE;
#dummie data for accounts
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (1,'PER',0,'sample account','2014-09-26 18:59:48');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (2,'PER',168,'sample account','2014-09-26 18:59:49');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (3,'PER',173,'sample account','2014-09-26 18:59:50');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (4,'PER',163,'sample account','2014-09-26 18:59:50');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (5,'PER',94,'sample account','2014-09-26 18:59:50');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (6,'PER',181,'sample account','2014-09-26 18:59:50');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (7,'PER',24,'sample account','2014-09-26 18:59:50');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (8,'PER',176,'sample account','2014-09-26 18:59:50');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (9,'PER',9,'sample account','2014-09-26 18:59:51');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (10,'PER',115,'sample account','2014-09-26 18:59:51');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (11,'PER',151,'sample account','2014-09-26 18:59:51');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (12,'PER',8,'sample account','2014-09-26 18:59:51');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (13,'PER',188,'sample account','2014-09-26 18:59:51');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (14,'PER',117,'sample account','2014-09-26 18:59:52');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (15,'PER',19,'sample account','2014-09-26 18:59:52');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (16,'PER',145,'sample account','2014-09-26 18:59:52');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (17,'PER',67,'sample account','2014-09-26 18:59:52');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (18,'PER',103,'sample account','2014-09-26 18:59:52');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (19,'PER',111,'sample account','2014-09-26 19:01:07');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (20,'PER',45,'sample account','2014-09-26 19:01:18');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (21,'PER',93,'sample account','2014-09-26 19:01:18');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (22,'PER',132,'sample account','2014-09-26 19:01:19');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (23,'PER',180,'sample account','2014-09-26 19:01:19');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (24,'PER',102,'sample account','2014-09-26 19:01:20');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (25,'PER',173,'sample account','2014-09-26 19:01:20');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (26,'PER',156,'sample account','2014-09-26 19:01:20');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (27,'PER',64,'sample account','2014-09-26 19:01:20');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (28,'PER',49,'sample account','2014-09-26 19:01:20');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (29,'PER',54,'sample account','2014-09-26 19:01:21');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (30,'PER',123,'sample account','2014-09-26 19:01:21');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (31,'PER',53,'sample account','2014-09-26 19:01:21');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (32,'PER',97,'sample account','2014-09-26 19:01:21');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (33,'PER',126,'sample account','2014-09-26 19:01:22');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (34,'PER',139,'sample account','2014-09-26 19:01:22');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (35,'PER',118,'sample account','2014-09-26 19:01:22');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (36,'PER',173,'sample account','2014-09-26 19:01:22');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (37,'PER',111,'sample account','2014-09-26 19:01:22');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (38,'PER',35,'sample account','2014-09-26 19:01:22');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (39,'PER',41,'sample account','2014-09-26 19:01:23');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (40,'PER',101,'sample account','2014-09-26 19:01:23');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (41,'PER',182,'sample account','2014-09-26 19:01:23');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (42,'PER',6,'sample account','2014-09-26 19:01:23');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (43,'PER',83,'sample account','2014-09-26 19:01:24');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (44,'PER',197,'sample account','2014-09-26 19:02:03');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (45,'PER',138,'sample account','2014-09-26 19:02:04');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (46,'PER',98,'sample account','2014-09-26 19:02:04');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (47,'PER',74,'sample account','2014-09-26 19:02:04');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (48,'PER',78,'sample account','2014-09-26 19:02:05');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (49,'PER',167,'sample account','2014-09-26 19:02:31');
INSERT INTO `Account` (`AID`,`ATYPE`,`BAL`,`DESCRI`,`ODATE`) VALUES (50,'PER',0,'sample account','2014-09-26 19:02:43');
UNLOCK TABLES;



