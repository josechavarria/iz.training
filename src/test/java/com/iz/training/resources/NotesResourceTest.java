/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import com.iz.training.orm.Notes;
import com.iz.training.schema.NoteDetails;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author josechavarria
 */
public class NotesResourceTest {
    
    public NotesResourceTest() {
       new NotesResource();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getbyId method, of class NotesResource.
     *//*
    @Test
    public void testGetbyId() {
        System.out.println("getbyId");
        BigInteger cid = BigInteger.valueOf(1);
        NotesResource instance = new NotesResource();
        long expResult = cid.longValue();
        long result = instance.getbyId(cid).getCid();
        assertNotEquals(expResult,result);
      
    }

    /**
     * Test of parseObject method, of class NotesResource.
     *//*
    @Test
    public void testParseObject() {
        System.out.println("parseObject");
        Notes classOrm = new Notes();
        classOrm.setCid((long)12);
        classOrm.setDatecreation(new Date());
        classOrm.setNid((long)12);
        classOrm.setTex("Junit Test");
        NotesResource instance = new NotesResource();
        String expResult = classOrm.getTex();
        String result = instance.parseObject(classOrm).getTEX();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of parseDetails method, of class NotesResource.
     *//*
    @Test
    public void testParseDetails() throws DatatypeConfigurationException {
        System.out.println("parseDetails");
        NoteDetails classDetails = new NoteDetails();
        NotesResource instance = new NotesResource();
        classDetails.setCID(BigInteger.valueOf(9));
        classDetails.setCREATIONDATE(ParseUtil.ParseDate(new Date()));
        classDetails.setNID(BigInteger.valueOf(22));
        classDetails.setTEX("Parse test by Junit");
        String expResult = classDetails.getTEX();
        String result = instance.parseDetails(classDetails).getTex();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of SaveNewObject method, of class NotesResource.
     *//*
    @Test
    public void testSaveNewObject() {
        System.out.println("SaveNewObject");
        Notes classOrm = new Notes();
        classOrm.setCid(23);
        classOrm.setTex("Created by JunitTest");
        NotesResource instance = new NotesResource();
        Long expResult = (long)1;
        Long result = instance.SaveNewObject(classOrm);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
     
    }

    /**
     * Test of UpdateObject method, of class NotesResource.
     *//*
    @Test
    public void testUpdateObject() {
        System.out.println("UpdateObject");
        Notes classOrm = new Notes();
        classOrm.setNid((long)2);
        classOrm.setTex("Updated by Junit");
        NotesResource instance = new NotesResource();
        Integer expResult = 1;
        Integer result = instance.UpdateObject(classOrm);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of verifyObjectId method, of class NotesResource.
     *//*
    @Test
    public void testVerifyObjectId() {
        System.out.println("verifyObjectId");
        Notes classOrm = null;
        NotesResource instance = new NotesResource();
        Integer expResult = null;
        Integer result = instance.verifyObjectId(classOrm);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAll method, of class NotesResource.
     *//*
    @Test
    public void testGetAll() {
        System.out.println("getAll");
        NotesResource instance = new NotesResource();
        
        long result = instance.getAll().size();
        long expResult = 0;
        assertNotEquals(expResult, result);
      
    }

    /**
     * Test of getByCustomer method, of class NotesResource.
     *//*
    @Test
    public void testGetByCustomer() {
        System.out.println("getByCustomer");
        BigInteger CustomerId = BigInteger.valueOf(22);
        NotesResource instance = new NotesResource();
        long expResult = 0;
        long result = instance.getByCustomer(CustomerId).size();
        assertNotEquals(expResult, result);
       
    }

    /**
     * Test of deleteNotes method, of class NotesResource.
     *//*
    @Test
    public void testDeleteNotes() {
        System.out.println("deleteNotes");
        BigInteger id = BigInteger.valueOf(3);
        NotesResource instance = new NotesResource();
        boolean expResult = true;
        boolean result = instance.deleteNotes(id);
        System.out.println(result);
        assertEquals(expResult, result);
        
    }
    */
}
