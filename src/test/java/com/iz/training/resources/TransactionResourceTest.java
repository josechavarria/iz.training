/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import com.iz.training.orm.Transact;
import com.iz.training.schema.TransactionDetails;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.xml.datatype.DatatypeConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author josechavarria
 */
public class TransactionResourceTest {
    
    public TransactionResourceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        new TransactionResource();
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getbyId method, of class TransactionResource.
     *//*
    @Test
    public void testGetbyId() {
        System.out.println("getbyId");
        BigInteger cid = BigInteger.valueOf(9);
        TransactionResource instance = new TransactionResource();
        long expResult = 0;
        long result = instance.getbyId(cid).getAcidd();
        assertNotEquals(expResult, result);
        
    }
     @Test
    public void testGetbyCustomerId() {
        System.out.println("getbyId");
        BigInteger cid = BigInteger.valueOf(21);
        TransactionResource instance = new TransactionResource();
       int expResult = 0;
        int result = instance.getByCustomer(cid).size();
        assertNotEquals(expResult, result);
        
    }
    /**
     * Test of getbyId method, of class TransactionResource.
     *//*
    @Test
    public void testGetbyAccount() {
        System.out.println("getbyAccount");
        BigInteger cid = BigInteger.valueOf(21);
        TransactionResource instance = new TransactionResource();
        Integer expResult = 0;
        Integer result = instance.getByAccount(cid).size();
        assertNotEquals(expResult, result);
        
    }

     @Test
    public void testGetAll() {
        System.out.println("getAll");
        BigInteger cid = null;
        TransactionResource instance = new TransactionResource();
        int expResult = 0;
        int result = instance.getAll().size();
        assertNotEquals(expResult, result);
        
    }
    /**
     * Test of parseObject method, of class TransactionResource.
     *//*
    @Test
    public void testParseObject() {
        System.out.println("parseObject");
        Transact classOrm = new Transact();
        classOrm.setAcidd(18);
       classOrm.setAcido(19);
       classOrm.setTamount(BigDecimal.valueOf(12.3));
       classOrm.setTdate(new Date());
       classOrm.setTid((long)12);
        TransactionResource instance = new TransactionResource();
         long expResult = classOrm.getAcidd();
         System.out.println(classOrm.getAcidd());
        BigInteger result = instance.parseObject(classOrm).getCIDD();  
        assertEquals(expResult,result.longValue());
      
    }

    /**
     * Test of parseDetails method, of class TransactionResource.
     *//*
    @Test
    public void testParseDetails() throws DatatypeConfigurationException {
        System.out.println("parseDetails");
        TransactionDetails classDetails = new TransactionDetails();
        classDetails.setCIDD(BigInteger.valueOf(22));
        classDetails.setCIDO(BigInteger.valueOf(21));
        classDetails.setDESCRI(null);
        classDetails.setTAMOUNT(BigDecimal.valueOf(0.4));
        classDetails.setTDATE(ParseUtil.ParseDate(new Date()));
        classDetails.setTID(BigInteger.valueOf(12));
        TransactionResource instance = new TransactionResource();
        BigInteger expResult = classDetails.getCIDD();
       long result = instance.parseDetails(classDetails).getAcidd();
        assertEquals(expResult.longValue(), result);
       
    }

    /**
     * Test of SaveNewObject method, of class TransactionResource.
     *//*
    @Test
    public void testSaveNewObject() {
        System.out.println("SaveNewObject");
          Transact classOrm = new Transact();
        classOrm.setAcidd(19);
       classOrm.setAcido(18);
       classOrm.setTamount(BigDecimal.valueOf(12.3));
       classOrm.setTdate(null);
        TransactionResource instance = new TransactionResource();
        Long expResult = (long)1;
        Long result = instance.SaveNewObject(classOrm);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of UpdateObject method, of class TransactionResource.
     *//*
    @Test
    public void testUpdateObject() {
        System.out.println("UpdateObject");
        Transact classOrm = null;
        TransactionResource instance = new TransactionResource();
        Integer expResult = null;
        Integer result = instance.UpdateObject(classOrm);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of verifyObjectId method, of class TransactionResource.
     *//*
    @Test
    public void testVerifyObjectId() {
        System.out.println("verifyObjectId");
        Transact classOrm = null;
        TransactionResource instance = new TransactionResource();
        Integer expResult = null;
        Integer result = instance.verifyObjectId(classOrm);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

   */ 
   
}
