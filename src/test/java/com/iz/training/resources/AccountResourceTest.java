/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import com.iz.training.orm.Account;
import com.iz.training.schema.AccountDetails;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.xml.datatype.DatatypeConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author josechavarria
 */
public class AccountResourceTest {
    
    public AccountResourceTest() {
        new AccountResource();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getbyId method, of class AccountResource.
     *//*
    @Test
    public void testGetbyId() {
        System.out.println("getbyId");
        BigInteger cid = BigInteger.valueOf(21);
        AccountResource instance = new AccountResource();
        Long expResult = (long) 21;
        Long result = instance.getbyId(cid).getAid();
        assertEquals(expResult, result);

    }

    /**
     * Test of parseObject method, of class AccountResource.
     *//*
    @Test
    public void testParseObject() {
        System.out.println("parseObject");
        Account classOrm = new Account();
        classOrm.setAid(Long.valueOf(12));
        classOrm.setAtype("PER");
        classOrm.setBal(BigDecimal.valueOf(12345.1));
        classOrm.setDescri("My account");
        classOrm.setOdate(new Date());
        AccountResource instance = new AccountResource();
        BigInteger expResult = BigInteger.valueOf(12);
        BigInteger result = instance.parseObject(classOrm).getACCN();
        assertEquals(expResult, result);
    }

    /**
     * Test of parseDetails method, of class AccountResource.
     * @throws javax.xml.datatype.DatatypeConfigurationException
     *//*
    @Test
    public void testParseDetails() throws DatatypeConfigurationException {
        System.out.println("parseDetails");
        AccountDetails classDetails = new AccountDetails();
        classDetails.setACCN(BigInteger.valueOf(12));
        classDetails.setATYPE("PER");
        classDetails.setDESCRI("description of account");
        classDetails.setODATE(ParseUtil.ParseDate(new Date()));
        AccountResource instance = new AccountResource();
        Long expResult = classDetails.getACCN().longValue();
        Long result = instance.parseDetails(classDetails).getAid();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of SaveNewObject method, of class AccountResource.
     *//*
    @Test
    public void testSaveNewObject() {
        System.out.println("SaveNewObject");
        Account classOrm = new Account("PER", BigDecimal.valueOf(1234.1), "My account", null);
        AccountResource instance = new AccountResource();
       Long expResult =(long) 1;
        Long result = instance.SaveNewObject(classOrm);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of UpdateObject method, of class AccountResource.
     *//*
    @Test
    public void testUpdateObject() {
        System.out.println("UpdateObject");
        Account classOrm = new Account("PER", BigDecimal.valueOf(1234.1), "My account", null);
       classOrm.setAid((long)21);
        AccountResource instance = new AccountResource();
        Integer expResult = 1;
        Integer result = instance.UpdateObject(classOrm);
      
    }

    /**
     * Test of verifyObjectId method, of class AccountResource.
     *//*
    @Test
    public void testVerifyObjectId() {
        System.out.println("verifyObjectId");
        Account a = new Account("PER", BigDecimal.valueOf(1234.1), "My account", null);
        a.setAid((long)21);
        AccountResource instance = new AccountResource();
        Integer expResult = 1;
        Integer result = instance.verifyObjectId(a);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of AgregateAccountCustomer method, of class AccountResource.
     *//*
    @Test
    public void testAgregateAccountCustomer() {
        System.out.println("AgregateAccountCustomer");
        BigInteger customerId = BigInteger.valueOf(21);
        BigInteger accountId = BigInteger.valueOf(22);
        AccountResource instance = new AccountResource();
        Integer expResult = 1;
        Integer result = instance.AgregateAccountCustomer(customerId, accountId);
        assertEquals(expResult, result);
        
    }*/
    
}
