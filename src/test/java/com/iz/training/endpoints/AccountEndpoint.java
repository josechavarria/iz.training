/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.endpoints;

import com.iz.training.orm.Account;
import com.iz.training.resources.AccountResource;
import com.iz.training.schema.AccountDetails;
import com.iz.training.schema.servicesoperations.GetAccountRequest;
import com.iz.training.schema.servicesoperations.GetAccountResponse;
import com.iz.training.schema.servicesoperations.NewAccountRequest;
import com.iz.training.schema.servicesoperations.NewAccountResponse;
import com.iz.training.schema.servicesoperations.SetAggregateAccountCustomerRequest;
import com.iz.training.schema.servicesoperations.SetAggregateAccountCustomerResponse;
import com.iz.training.schema.servicesoperations.UpdateAccountRequest;
import com.iz.training.schema.servicesoperations.UpdateAccountResponse;
import java.math.BigInteger;
import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 *
 * @author josechavarria
 */
@Endpoint
public class AccountEndpoint {

    private static final String NAMESPACE_URI = "http://com/iz/training/schema/ServicesOperations";
    private AccountResource accountResource;

    @Autowired
    public AccountEndpoint(AccountResource accountResource) throws JDOMException {
        this.accountResource=accountResource;
    }
    @PayloadRoot(localPart = "SetAggregateAccountCustomerRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public SetAggregateAccountCustomerResponse accountResponse(@RequestPayload SetAggregateAccountCustomerRequest setAggregateAccountCustomerRequest)
            throws Exception {
        AccountDetails account = null;
        SetAggregateAccountCustomerResponse response = new SetAggregateAccountCustomerResponse();
        Integer feedback=this.accountResource.AgregateAccountCustomer(setAggregateAccountCustomerRequest.getCID(), setAggregateAccountCustomerRequest.getAID());
        if(feedback==-5||feedback==-1){
            response.setFEEDBACK("Cannot update account to database");
        }else if(feedback==1){
            response.setFEEDBACK("Account updated successfully! ");
        }else if(feedback==-2){
            response.setFEEDBACK("Account/Customer relationship already exists! ");
        }else if(feedback ==-4){
            response.setFEEDBACK("Account with id : "+setAggregateAccountCustomerRequest.getAID()+" does not exists!");
        }else if(feedback ==-3){
            response.setFEEDBACK("Customer with id : "+setAggregateAccountCustomerRequest.getCID()+" does not exists!");
        }
        return response;

    }
    @PayloadRoot(localPart = "GetAccountRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public GetAccountResponse accountResponse(@RequestPayload GetAccountRequest getAccountRequest)
            throws Exception {
        AccountDetails account = null;
        GetAccountResponse accountResponse = new GetAccountResponse();
        account=  this.accountResource.parseObject(accountResource.getbyId(getAccountRequest.getCID()));
        accountResponse.setAccountDetails(account);
        return accountResponse;

    }
    @PayloadRoot(localPart="UpdateAccountRequest",namespace=NAMESPACE_URI)
    @ResponsePayload
    public UpdateAccountResponse updateAccountResponse(@RequestPayload UpdateAccountRequest UpdateAccountRequest){
        UpdateAccountResponse response=new UpdateAccountResponse();
        Account account=this.accountResource.parseDetails(UpdateAccountRequest.getAccountDetails());
        Integer feedback=accountResource.UpdateObject(account);
        if(feedback==-5||feedback==-1){
            response.setFEEDBACK("Cannot update account to database");
            
        }else if(feedback==1){
            response.setFEEDBACK("Account updated successfully! ");
        }else if(feedback ==-4){
            response.setFEEDBACK("Customer with CID "+account.getAid()+" does not exist!");
        }
        return response;
    }

    @PayloadRoot(localPart="NewAccountRequest",namespace=NAMESPACE_URI)
    @ResponsePayload
    public NewAccountResponse newAccountResponse(@RequestPayload NewAccountRequest newAccountRequest){
        NewAccountResponse response=new NewAccountResponse();
        Account account=null;
        account= this.accountResource.parseDetails(newAccountRequest.getAccountDetails());
        account.setAid(null);//force the insert null to auto_increment
        Long feedback=accountResource.SaveNewObject(account);
        if(feedback==-1){
            response.setFEEDBACK("Cannot add account to database");
            
        }else if(feedback>0){
            response.setFEEDBACK("Account with Id: "+feedback+" added successfully! ");
        }
        return response;
    }
    
}
