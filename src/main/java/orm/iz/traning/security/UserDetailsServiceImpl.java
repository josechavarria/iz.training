/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orm.iz.traning.security;

import com.iz.training.orm.Autentication;
import freemarker.log.Logger;
import java.math.BigInteger;
import java.util.logging.Level;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AutenticationResource autenticationResource;
    @Autowired
    private Assembler assembler;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        UserDetails userDetails = null;
        Autentication userEntity = null;
        if (username.matches("\\d+")) {
            userEntity = autenticationResource.getUserById(BigInteger.valueOf(Long.getLong(username)));
            userDetails = assembler.buildUserFromUserEntity(userEntity);
        } else {
            throw new UsernameNotFoundException("InvalidUsername");

        }
        if (userEntity == null || userEntity.getRole() == null && userEntity.getCid() < 1) {
            throw new UsernameNotFoundException("user not found");

        }

        return userDetails;
    }

}
