/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orm.iz.traning.security;

import com.iz.training.orm.Autentication;
import com.iz.training.resources.CustomerResource;
import java.util.ArrayList;
import java.util.Collection;
import javax.transaction.Transactional;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service("assembler")
public class Assembler {


  public User buildUserFromUserEntity(Autentication a) {

    String Username = String.valueOf(a.getCid());
    String password = a.getPass();
    boolean enabled = AutenticationResource.isActive(a);
    boolean accountNonExpired = enabled;
    boolean credentialsNonExpired = enabled;
    boolean accountNonLocked = enabled;

    Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
   
      authorities.add(new GrantedAuthorityImpl(a.getRole()));
    

    User user = new User(Username, password, enabled,accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    return user;
  }
}