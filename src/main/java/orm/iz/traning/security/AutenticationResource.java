/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orm.iz.traning.security;

import com.iz.training.orm.Autentication;
import com.iz.training.resources.CustomerResource;
import com.iz.training.resources.HibernateUtil;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Hibernate;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 *
 * @author josechavarria
 */
public class AutenticationResource {

    private final PasswordEncoder passwordEncoder;

    public AutenticationResource() {
        HibernateUtil.getSessionFactory();
        this.passwordEncoder = new ShaPasswordEncoder();
    }

    public Autentication getUser(BigInteger user, String password) {

        Session ses = HibernateUtil.getSessionFactory().openSession();
        List<Autentication> al = new ArrayList();
        Autentication a;
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            ses.beginTransaction();
            al = (List<Autentication>) ses.get(Autentication.class, user.longValue());
            Hibernate.initialize(al);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(AutenticationResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();
            al = new ArrayList();
        }
        if (al.size() == 1 && al.get(0).getPass().equals(password)) {
            a = al.get(0);
        } else {
            a = new Autentication();
        }
        a.setPass(null);
        return a;
    }

    public Autentication getUserById(BigInteger user) {

        Session ses = HibernateUtil.getSessionFactory().openSession();
        List<Autentication> al = new ArrayList();
        Autentication a;
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            ses.beginTransaction();
            al = (List<Autentication>) ses.get(Autentication.class, user.longValue());
            Hibernate.initialize(al);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(AutenticationResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();
            al = new ArrayList();
        }
        if (al.size() == 1) {
            a = al.get(0);
        } else {
            a = new Autentication();
        }
        a.setPass(null);
        return a;
    }

    public Integer changePassword(BigInteger user, String oldPassword, String password1, String password2) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Integer result;
        try {
            if (user == null && oldPassword == null && password1 == null && password2 == null) {
                result = -1;
            } else if ((!oldPassword.equals(password1)) && !oldPassword.equals(password2) && password1.equals(password2)) {

                Autentication a = new Autentication();

                a = (Autentication) ses.get(Autentication.class, user.longValue());
                Hibernate.initialize(a);
                a.setPass(passwordEncoder.encodePassword(password2, null));
                ses.beginTransaction();
                ses.update(a);
                ses.getTransaction().commit();
                result = 1;
            } else {
                result = -2;
            }
        } catch (Exception ex) {
            ses.getTransaction().rollback();
            result = -3;
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        ses.close();
        return result;

    }

    public boolean registAttemps(BigInteger user, Integer attemps) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Boolean result;
        try {
            Autentication a = new Autentication();
            a = (Autentication) ses.get(Autentication.class, user.longValue());
            Hibernate.initialize(a);
            ses.beginTransaction();
            ses.update(a);
            ses.getTransaction().commit();
            result = true;
        } catch (Exception ex) {
            ses.getTransaction().rollback();
            result = false;
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        ses.close();
        return result;

    }

    public Integer getAttemps(BigInteger User) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Integer result;
        try {

            Autentication a = (Autentication) ses.get(Autentication.class, User.longValue());
            Hibernate.initialize(a);
            result = a.getAttemps();
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();
            result = -2;
        }
        return result;
    }

    public static boolean isActive(Autentication a) {

        return a.getAttemps() < 3;

    }
}
