/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.endpoints;

import com.iz.training.orm.Customer;
import com.iz.training.resources.CustomerResource;
import com.iz.training.resources.Resource;
import com.iz.training.schema.CustomerDetails;
import com.iz.training.schema.servicesoperations.GetCustomerRequest;
import com.iz.training.schema.servicesoperations.GetCustomerResponse;
import com.iz.training.schema.servicesoperations.NewCustomerRequest;
import com.iz.training.schema.servicesoperations.NewCustomerResponse;
import com.iz.training.schema.servicesoperations.UpdateCustomerRequest;
import com.iz.training.schema.servicesoperations.UpdateCustomerResponse;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class CustomerEndPoint {

   
    private static final String NAMESPACE_URI = "http://com/iz/training/schema/ServicesOperations";
    private CustomerResource customerResource;

    @Autowired
    public CustomerEndPoint(CustomerResource customerResource) throws JDOMException {
        this.customerResource=customerResource;
    }

    @PayloadRoot(localPart = "GetCustomerRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public GetCustomerResponse customerResponse(@RequestPayload GetCustomerRequest getCustomerRequest)
            throws Exception {
        CustomerDetails customer = null;
        GetCustomerResponse customerResponse = new GetCustomerResponse();
        customer=  this.customerResource.parseObject(customerResource.getbyId(getCustomerRequest.getCID()));
        customerResponse.setCustomerDetails(customer);
        return customerResponse;

    }
    @PayloadRoot(localPart="UpdateCustomerRequest",namespace=NAMESPACE_URI)
    @ResponsePayload
    public UpdateCustomerResponse updateCustomerResponse(@RequestPayload UpdateCustomerRequest UpdateCustomerRequest){
        UpdateCustomerResponse response=new UpdateCustomerResponse();
        Customer customer=this.customerResource.parseDetails(UpdateCustomerRequest.getCustomerDetails());
        Integer feedback=customerResource.UpdateObject(customer);
        if(feedback==-1){
            response.setFEEDBACK("Cannot update customer to database");
            
        }else if(feedback==1){
            response.setFEEDBACK("Customer updated successfully! ");
        } else if(feedback ==-4){
            response.setFEEDBACK("Customer with CID "+customer.getCid()+" does not exist!");
        }
        
        return response;
    }

    @PayloadRoot(localPart="NewCustomerRequest",namespace=NAMESPACE_URI)
    @ResponsePayload
    public NewCustomerResponse newCustomerResponse(@RequestPayload NewCustomerRequest newCustomerRequest){
        NewCustomerResponse response=new NewCustomerResponse();
        Customer customer=null;
        customer= this.customerResource.parseDetails(newCustomerRequest.getCustomerDetails());
        customer.setCid(null);//force the insert null to auto_increment
        Long feedback=customerResource.SaveNewObject(customer);
        if(feedback==-1){
            response.setFEEDBACK("Cannot add customer to database");
            
        }else if(feedback>1){
            response.setFEEDBACK("Customer id: "+feedback+" added successfully! ");
        }
        return response;
    }
    
}
