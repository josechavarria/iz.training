/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.endpoints;

import com.iz.training.orm.Notes;
import com.iz.training.resources.NotesResource;
import com.iz.training.schema.NoteDetails;
import com.iz.training.schema.servicesoperations.DeleteNoteRequest;
import com.iz.training.schema.servicesoperations.DeleteNoteResponse;
import com.iz.training.schema.servicesoperations.GetNoteCustomerRequest;
import com.iz.training.schema.servicesoperations.GetNoteCustomerResponse;
import com.iz.training.schema.servicesoperations.GetNoteRequest;
import com.iz.training.schema.servicesoperations.GetNoteResponse;
import com.iz.training.schema.servicesoperations.NewNoteRequest;
import com.iz.training.schema.servicesoperations.NewNoteResponse;
import com.iz.training.schema.servicesoperations.UpdateNoteRequest;
import com.iz.training.schema.servicesoperations.UpdateNoteResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 *
 * @author josechavarria
 */
@Endpoint
public class NotesEndPoint {

    private static final String NAMESPACE_URI = "http://com/iz/training/schema/ServicesOperations";
    private NotesResource notesResource;

    @Autowired
    public NotesEndPoint(NotesResource notesResource) throws JDOMException {
        this.notesResource = notesResource;
    }

    @PayloadRoot(localPart = "GetNotesRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public GetNoteResponse notesResponse(@RequestPayload GetNoteRequest getNotesRequest)
            throws Exception {
        NoteDetails notes = null;
        GetNoteResponse notesResponse = new GetNoteResponse();
        notes = this.notesResource.parseObject(notesResource.getbyId(getNotesRequest.getNID()));
        notesResponse.setNoteDetails(notes);
        return notesResponse;

    }

    @PayloadRoot(localPart = "GetNoteCustomerRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public GetNoteCustomerResponse noteResponse(@RequestPayload GetNoteCustomerRequest getNoteCustomerRequest)
            throws Exception {
        List<NoteDetails> noteDetails = new ArrayList<NoteDetails>();

        GetNoteCustomerResponse noteCustomerResponse = new GetNoteCustomerResponse();
        List<Notes> noteOrm = notesResource.getByCustomer(getNoteCustomerRequest.getCID());
        if (noteOrm.size() == 0) {
            noteCustomerResponse.setFEEDBACK("The Customer with id " + getNoteCustomerRequest.getCID() + " does not have accounts!");
        } else {
            List<NoteDetails> td = notesResource.parseListNoteOrm(noteOrm);
            noteCustomerResponse.getNoteList().addAll(td);

        }
        return noteCustomerResponse;

    }

    @PayloadRoot(localPart = "NewNoteRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public NewNoteResponse newNotesResponse(@RequestPayload NewNoteRequest newNotesRequest) {
        NewNoteResponse response = new NewNoteResponse();
        Notes notes = null;
        notes = this.notesResource.parseDetails(newNotesRequest.getNoteDetails());
        notes.setNid(null);//force the insert null to auto_increment
        Long feedback = notesResource.SaveNewObject(notes);
        if (feedback == -1) {
            response.setFEEDBACK("Cannot add notes to database");
        } else if (feedback > 0) {
            response.setFEEDBACK("Notes id: " + feedback + " added successfully! ");
        }
        return response;
    }

    @PayloadRoot(localPart = "DeleteNoteRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public DeleteNoteResponse deleteNotesResponse(@RequestPayload DeleteNoteRequest deleteNotesRequest) {
        DeleteNoteResponse response = new DeleteNoteResponse();
        BigInteger note = deleteNotesRequest.getNID();
        Integer feedback = notesResource.deleteNotes(note);
        if (feedback == -11) {
            response.setFEEDBACK("Cannot add notes to database");
        } else if (feedback == 1) {
            response.setFEEDBACK("Notes id: " + note + " deleted successfully! ");
        } else if (feedback == -2) {
            response.setFEEDBACK("Notes id: " + note + " do not exists! ");
        }
        return response;
    }

    @PayloadRoot(localPart = "UpdateNoteRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public UpdateNoteResponse updateNotesResponse(@RequestPayload UpdateNoteRequest updateNotesRequest) {
        UpdateNoteResponse response = new UpdateNoteResponse();
        Notes n = this.notesResource.parseDetails(updateNotesRequest.getNoteDetails());
        System.out.println("" + n.getTex() + " " + n.getDatecreation() + " " + n.getNid());
        Integer feedback = notesResource.UpdateObject(n);
        if (feedback == 1) {
            response.setFEEDBACK("Notes id: " + updateNotesRequest.getNoteDetails().getNID() + " updated successfully! ");
        } else if (feedback == -4) {
            response.setFEEDBACK("Notes id: " + updateNotesRequest.getNoteDetails().getNID() + " do not exist!");
        } else {
            response.setFEEDBACK("Cannot update note");
        }
        return response;
    }
}
