/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.endpoints;

import com.iz.training.orm.Transact;
import com.iz.training.resources.TransactionResource;
import com.iz.training.schema.TransactionDetails;
import com.iz.training.schema.servicesoperations.GetTransactionAccountRequest;
import com.iz.training.schema.servicesoperations.GetTransactionAccountResponse;
import com.iz.training.schema.servicesoperations.GetTransactionCustomerRequest;
import com.iz.training.schema.servicesoperations.GetTransactionCustomerResponse;
import com.iz.training.schema.servicesoperations.GetTransactionRequest;
import com.iz.training.schema.servicesoperations.GetTransactionResponse;
import com.iz.training.schema.servicesoperations.NewTransactionRequest;
import com.iz.training.schema.servicesoperations.NewTransactionResponse;
import com.iz.training.schema.servicesoperations.UpdateNoteRequest;
import com.iz.training.schema.servicesoperations.UpdateNoteResponse;
import java.util.ArrayList;
import java.util.List;
import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 *
 * @
 */
@Endpoint
public class TransactionEndPoint {

    private static final String NAMESPACE_URI = "http://com/iz/training/schema/ServicesOperations";
    private TransactionResource transactionResource;

    @Autowired
    public TransactionEndPoint(TransactionResource transactionResource) throws JDOMException {
        this.transactionResource = transactionResource;
        System.out.println("TransactionEndpoint");
    }

    @PayloadRoot(localPart = "GetTransactionRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public GetTransactionResponse transactionResponse(@RequestPayload GetTransactionRequest getTransactionRequest)
            throws Exception {
        TransactionDetails transaction = null;
        GetTransactionResponse transactionResponse = new GetTransactionResponse();
        transaction = this.transactionResource.parseObject(transactionResource.getbyId(getTransactionRequest.getTID()));
        transactionResponse.setTransactDetails(transaction);
        return transactionResponse;

    }

    @PayloadRoot(localPart = "GetTransactionCustomerRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public GetTransactionCustomerResponse transactionResponse(@RequestPayload GetTransactionCustomerRequest getTransactionCustomerRequest)
            throws Exception {
        List<TransactionDetails> transactionDetails = new ArrayList<TransactionDetails>();

        GetTransactionCustomerResponse transactionCustomerResponse = new GetTransactionCustomerResponse();
        List<Transact> transactionOrm = transactionResource.getByCustomer(getTransactionCustomerRequest.getCID());
        if (transactionOrm.size() == 0) {
            transactionCustomerResponse.setFEEDBACK("The Customer with id " + getTransactionCustomerRequest.getCID() + " does not have Notes!");
        } else {
            List<TransactionDetails> td = transactionResource.parseListTransactionOrm(transactionOrm);
            transactionCustomerResponse.getTransactionsList().addAll(td);

        }
        return transactionCustomerResponse;

    }

    @PayloadRoot(localPart = "GetTransactionAccountRequest", namespace = NAMESPACE_URI)
    @ResponsePayload
    public GetTransactionAccountResponse transactionResponse(@RequestPayload GetTransactionAccountRequest getTransactionAccountRequest)
            throws Exception {
        List<TransactionDetails> transactionDetails = new ArrayList<TransactionDetails>();

        GetTransactionAccountResponse transactionAccountResponse = new GetTransactionAccountResponse();
        List<Transact> transactionOrm = transactionResource.getByAccount(getTransactionAccountRequest.getAID());
        if (transactionOrm.size() == 0) {
            transactionAccountResponse.setFEEDBACK("The Account with id " + getTransactionAccountRequest.getAID() + " does not have accounts!");
        } else {
            List<TransactionDetails> td = transactionResource.parseListTransactionOrm(transactionOrm);
            transactionAccountResponse.getTransactionsList().addAll(td);

        }
        return transactionAccountResponse;

    }

}
