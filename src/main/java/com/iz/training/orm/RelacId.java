package com.iz.training.orm;
// Generated Oct 8, 2014 12:50:38 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * RelacId generated by hbm2java
 */
@Embeddable
public class RelacId  implements java.io.Serializable {


     private long cid;
     private long aid;

    public RelacId() {
    }

    public RelacId(long cid, long aid) {
       this.cid = cid;
       this.aid = aid;
    }
   


    @Column(name="CID", nullable=false)
    public long getCid() {
        return this.cid;
    }
    
    public void setCid(long cid) {
        this.cid = cid;
    }


    @Column(name="AID", nullable=false)
    public long getAid() {
        return this.aid;
    }
    
    public void setAid(long aid) {
        this.aid = aid;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof RelacId) ) return false;
		 RelacId castOther = ( RelacId ) other; 
         
		 return (this.getCid()==castOther.getCid())
 && (this.getAid()==castOther.getAid());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + (int) this.getCid();
         result = 37 * result + (int) this.getAid();
         return result;
   }   


}


