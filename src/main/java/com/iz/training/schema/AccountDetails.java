//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.09 at 04:34:52 PM WEST 
//


package com.iz.training.schema;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AccountDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACCN" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
 *         &lt;element name="ATYPE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="PER"/>
 *               &lt;enumeration value="ASS"/>
 *               &lt;enumeration value="ENTE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DESCRI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BAL" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ODATE" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountDetails", propOrder = {
    "accn",
    "atype",
    "descri",
    "bal",
    "odate"
})
public class AccountDetails {

    @XmlElement(name = "ACCN", required = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger accn;
    @XmlElement(name = "ATYPE", required = true)
    protected String atype;
    @XmlElement(name = "DESCRI", required = true)
    protected String descri;
    @XmlElement(name = "BAL", required = true)
    protected BigDecimal bal;
    @XmlElement(name = "ODATE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar odate;

    /**
     * Gets the value of the accn property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getACCN() {
        return accn;
    }

    /**
     * Sets the value of the accn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setACCN(BigInteger value) {
        this.accn = value;
    }

    /**
     * Gets the value of the atype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getATYPE() {
        return atype;
    }

    /**
     * Sets the value of the atype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setATYPE(String value) {
        this.atype = value;
    }

    /**
     * Gets the value of the descri property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRI() {
        return descri;
    }

    /**
     * Sets the value of the descri property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRI(String value) {
        this.descri = value;
    }

    /**
     * Gets the value of the bal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBAL() {
        return bal;
    }

    /**
     * Sets the value of the bal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBAL(BigDecimal value) {
        this.bal = value;
    }

    /**
     * Gets the value of the odate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getODATE() {
        return odate;
    }

    /**
     * Sets the value of the odate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setODATE(XMLGregorianCalendar value) {
        this.odate = value;
    }

}
