/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import com.iz.training.orm.Account;
import com.iz.training.orm.Customer;
import com.iz.training.orm.Transact;
import com.iz.training.schema.TransactionDetails;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

/**
 *
 * @author josechavarria
 */
public class TransactionResource implements Resource<Transact, TransactionDetails> {

    public TransactionResource() {
        HibernateUtil.getSessionFactory();
    }

    @Override
    public Transact getbyId(BigInteger cid) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Transact t;
        try {
            ses.beginTransaction();
            t = (Transact) ses.get(Transact.class, cid.longValue());
            Hibernate.initialize(t);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();
            t = new Transact();
        }

        return t;

    }

    public List<Transact> getByCustomer(BigInteger CustomerId) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        List<Transact> t = new ArrayList<Transact>();
        try {
            String sql = "select Transact.* from Transact  inner join Account on Transact.ACIDD=Account.AID or Transact.ACIDO=Account.AID inner join relac ON Relac.AID=account.AID where Relac.CID=:CustomerId group by Transact.TID;";
            Query query = ses.createSQLQuery(sql)
                    .addEntity("Transact",Transact.class)
                    .setParameter("CustomerId", CustomerId);
            ses.beginTransaction(); 
            t.addAll((List<Transact>) query.list());
            Hibernate.initialize(t);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();

        }

        return t;

    }

    public List<Transact> getByAccount(BigInteger Accountid) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        List<Transact> t = new ArrayList<Transact>();
        try {
            Criterion origin = Restrictions.eq("acido", Accountid.longValue());
            Criterion destination = Restrictions.eq("acidd", Accountid.longValue());
            LogicalExpression expression = Restrictions.or(origin, destination);
            Criteria crit = ses.createCriteria(Transact.class);
            crit.add(expression);
            ses.beginTransaction();
            t.addAll((List<Transact>) crit.list());
            Hibernate.initialize(t);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();

        }

        return t;

    }

    /**
     *
     * @return
     */
    @Override
    public List<Transact> getAll() {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        List<Transact> t = new ArrayList<Transact>();
        try {
            ses.beginTransaction();
            t.addAll((List<Transact>) ses.createCriteria(Transact.class).list());
            Hibernate.initialize(t);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();

        }
        return t;
    }

    @Override
    public TransactionDetails parseObject(Transact classOrm) {
        TransactionDetails td = new TransactionDetails();
        try {
            td.setCIDD(BigInteger.valueOf(classOrm.getAcidd()));
            td.setCIDO(BigInteger.valueOf(classOrm.getAcido()));
            td.setTAMOUNT(classOrm.getTamount());
            td.setTDATE(ParseUtil.ParseDate(classOrm.getTdate()));
            td.setTID(classOrm.getTid() != null ? BigInteger.valueOf(classOrm.getTid()) : null);

        } catch (Exception ex) {
            td = new TransactionDetails();
            Logger.getLogger(TransactionResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return td;
    }

    @Override
    public Transact parseDetails(TransactionDetails classDetails) {
        Transact t = new Transact();
        try {
            
            t.setAcidd(classDetails.getCIDD().longValue());
            t.setAcido(classDetails.getCIDO().longValue());
            t.setTamount(classDetails.getTAMOUNT());
            t.setTdate(classDetails.getTDATE()!=null?ParseUtil.ParseXMLDate(classDetails.getTDATE()):null);
            t.setTid(classDetails.getTID() != null ? classDetails.getTID().longValue() : null);

        } catch (Exception ex) {
            t = new Transact();
            Logger.getLogger(TransactionResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return t;
    }

 
    public Integer doTransaction(Transact classOrm) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Integer result;
        try {
            ses.beginTransaction();
            Query query = ses.createSQLQuery("CALL  `customer`.`DOTRANSACTION`(:AMNT,:AIDD, :AIDO);")
                    .setParameter("AMNT", classOrm.getTamount())
                    .setParameter("AIDD", classOrm.getAcidd())
                    .setParameter("AIDO", classOrm.getAcido());  
            result = (Integer) query.uniqueResult();
            ses.getTransaction().commit();
            ses.close();

        } catch (Exception ex) {
            Logger.getLogger(AccountResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.getTransaction().rollback();
            ses.close();
            result =  -6; 
        }
        return result;
    }

    @Override
    public Integer UpdateObject(Transact classOrm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer verifyObjectId(Transact classOrm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<TransactionDetails> parseListTransactionOrm(List<Transact> list) {
        List<TransactionDetails> result = new ArrayList<TransactionDetails>();
        System.out.println(" v"+list.toString());;
        try {
            for (Transact t : list) {

                result.add(this.parseObject(t));
            }
            System.out.println(result.size() + " vs " + list.size());
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            result = new ArrayList();
        }

        return result;
    }

    public List<Transact> parseListTransactionDetails(List<TransactionDetails> list) {
        List<Transact> result = new ArrayList();
        try {
            for (TransactionDetails t : list) {
                result.add(this.parseDetails(t));
            }
            System.out.println(result.size() + " vs " + list.size());
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            result = new ArrayList();
        }

        return result;

    }

    @Override
    public Long SaveNewObject(Transact classOrm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
