/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import com.iz.training.orm.Account;
import com.iz.training.orm.Customer;
import com.iz.training.schema.CustomerDetails;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author josechavarria
 * @param <classOrm>
 * @param <classDetails>
 */
public interface Resource<ClassOrm, ClassDetails> {


    public ClassOrm getbyId(BigInteger cid);

    public ClassDetails parseObject(ClassOrm classOrm);

    public ClassOrm parseDetails(ClassDetails classDetails);

    public Long SaveNewObject(ClassOrm classOrm);

    public Integer UpdateObject(ClassOrm classOrm);

    public Integer verifyObjectId(ClassOrm classOrm);
   public List<ClassOrm> getAll();
}
