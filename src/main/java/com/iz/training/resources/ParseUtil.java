/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author josechavarria
 */
public class ParseUtil {
      public static XMLGregorianCalendar ParseDate(Date d) throws DatatypeConfigurationException {
           if (d == null) {
            return null;
        }
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(d);
        XMLGregorianCalendar XMLDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        return XMLDate;

    }

    public static Date ParseXMLDate(XMLGregorianCalendar XMLDate) {
        if (XMLDate == null) {
            return null;
        }
        return XMLDate.toGregorianCalendar().getTime();

    }
}
