/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import com.iz.training.orm.Account;
import com.iz.training.schema.AccountDetails;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author josechavarria
 */
public class AccountResource implements Resource<Account, AccountDetails> {

    public AccountResource() {
        HibernateUtil.getSessionFactory();
    }

    @Override
    public Account getbyId(BigInteger cid) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Account a = new Account();
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            ses.beginTransaction();
            a = (Account) ses.get(Account.class, cid.longValue());
            Hibernate.initialize(a);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(AccountResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();
            a = new Account();
        }
        return a;
    }

    @Override
    public AccountDetails parseObject(Account classOrm) {
        AccountDetails ad = new AccountDetails();
        try {
            ad.setACCN(BigInteger.valueOf(classOrm.getAid()));
            ad.setATYPE(classOrm.getAtype());
            ad.setBAL(classOrm.getBal());
            ad.setDESCRI(classOrm.getDescri());
            ad.setODATE(ParseUtil.ParseDate(classOrm.getOdate()));
        } catch (Exception ex) {
            Logger.getLogger(AccountResource.class.getName()).log(Level.SEVERE, null, ex);
            ad = new AccountDetails();
        }
        return ad;
    }

    @Override
    public Account parseDetails(AccountDetails classDetails) {
        Account a = new Account();
        try {
            a.setAid(classDetails.getACCN() != null ? classDetails.getACCN().longValue() : null);
            a.setAtype(classDetails.getATYPE());
            a.setBal(classDetails.getBAL());
            a.setDescri(classDetails.getDESCRI());
            a.setOdate(classDetails.getODATE() != null ? ParseUtil.ParseXMLDate(classDetails.getODATE()) : null);
        } catch (Exception ex) {
            Logger.getLogger(AccountResource.class.getName()).log(Level.SEVERE, null, ex);
            a = new Account();
        }
        return a;
    }

    @Override
    public Long SaveNewObject(Account classOrm) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Long result;
        try {
            ses.beginTransaction();
            ses.save(classOrm);
            ses.getTransaction().commit();
            ses.close();
            result = classOrm.getAid();
        } catch (Exception ex) {
            Logger.getLogger(AccountResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.getTransaction().rollback();
            ses.close();
            result =(long) -1;
        }
        return result;
    }

    @Override
    public Integer UpdateObject(Account classOrm) {
        Session ses = HibernateUtil.getSessionFactory().openSession();

        Integer result;
        try {
            ses.beginTransaction();
            Query query = ses.createSQLQuery("CALL UpdateAccount(:typ,:descr,:id) ;")
                    .setParameter("typ", classOrm.getAtype())
                    .setParameter("descr", classOrm.getDescri())
                    .setParameter("id", classOrm.getAid());
            result = (Integer) query.uniqueResult();
            ses.getTransaction().commit();
            ses.close();     
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.getTransaction().rollback();
            ses.close();
            result=-2;
        }
        return result;
    }

    @Override
    public Integer verifyObjectId(Account a) {
        BigInteger result;
         Session ses = HibernateUtil.getSessionFactory().openSession();
        try{
        result = (BigInteger) ses.createSQLQuery("Select verifyAccount(:AIDV)").setParameter("AIDV", a.getAid()).uniqueResult();
        }catch(Exception ex){
        
        result=BigInteger.valueOf(-1);
        }
        ses.close();
        return result.intValue();
    }

    public Integer AgregateAccountCustomer(BigInteger customerId, BigInteger accountId) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Integer result;
        try {
            Query query = ses.createSQLQuery("CALL AggregateCustomerAccount(:c, :a)  ;")
                    .setParameter("c", customerId)
                    .setParameter("a", accountId);
            result = (Integer) query.uniqueResult();
            ses.close();

        } catch (Exception ex) {
            Logger.getLogger(AccountResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.getTransaction().rollback();
            ses.close();
            result = -2;
        }
        return result;
    }

    @Override
    public List<Account> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
