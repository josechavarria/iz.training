/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import com.iz.training.orm.Notes;
import com.iz.training.orm.Transact;
import com.iz.training.schema.NoteDetails;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author josechavarria
 */
public class NotesResource implements Resource<Notes, NoteDetails> {

    public NotesResource() {
        HibernateUtil.getSessionFactory();
    }

    /**
     *
     * @param cid
     * @return
     */
    @Override
    public Notes getbyId(BigInteger cid) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Notes n;
        n = new Notes();
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            ses.beginTransaction();
            n = (Notes) ses.get(Notes.class, cid.longValue());
            Hibernate.initialize(n);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(AccountResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();
            n = new Notes();
        }
        return n;
    }

    /**
     *
     * @param classOrm
     * @return
     */
    @Override
    public NoteDetails parseObject(Notes classOrm) {
        NoteDetails nd = new NoteDetails();
        try {
            nd.setTEX(classOrm.getTex());
            nd.setCID(BigInteger.valueOf(classOrm.getCid()));
            nd.setNID(classOrm.getNid() != null ? BigInteger.valueOf(classOrm.getNid()) : null);
            nd.setCREATIONDATE(ParseUtil.ParseDate(classOrm.getDatecreation()));
        } catch (Exception ex) {
            Logger.getLogger(NotesResource.class.getName()).log(Level.SEVERE, null, ex);
            nd = new NoteDetails();
        }
        return nd;
    }

    /**
     *
     * @param classDetails
     * @return
     */
    @Override
    public Notes parseDetails(NoteDetails classDetails) {
        Notes n = new Notes();
        try {
            System.out.println("" + classDetails.getCID());
            n.setCid(classDetails.getCID() != null ? classDetails.getCID().longValue() : null);
            n.setDatecreation(classDetails.getCREATIONDATE() != null ? ParseUtil.ParseXMLDate(classDetails.getCREATIONDATE()) : null);
            n.setNid(classDetails.getNID() != null ? classDetails.getNID().longValue() : null);
            n.setTex(classDetails.getTEX());
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            n = new Notes();
        }
        return n;
    }

    /**
     *
     * @param classOrm
     * @return
     */
    @Override
    public Long SaveNewObject(Notes classOrm) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Long result;
        try {
            ses.beginTransaction();
            ses.save(classOrm);
            ses.getTransaction().commit();
            ses.close();
            result = classOrm.getNid();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            ses.getTransaction().rollback();
            ses.close();
            result = (long) -1;
        }
        return result;

    }

    /**
     *
     * @param classOrm
     * @return
     */
    @Override
    public Integer UpdateObject(Notes classOrm) {
        Session ses = HibernateUtil.getSessionFactory().openSession();

        Integer result;
        try {
            System.out.println(classOrm.getTex());
            ses.beginTransaction();
            Query query = ses.createSQLQuery("CALL UpdateNotes(:te,:id) ;")
                    .setParameter("te", classOrm.getTex())
                    .setParameter("id", BigInteger.valueOf(classOrm.getNid()));
            result = (Integer) query.uniqueResult();
            ses.getTransaction().commit();
            ses.close();

        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.getTransaction().rollback();
            ses.close();
            result = -2;
        }
        return result;

    }

    /**
     *
     * @param classOrm
     * @return
     */
    @Override
    public Integer verifyObjectId(Notes classOrm) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    @Override
    public List<Notes> getAll() {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        List<Notes> n = new ArrayList<Notes>();
        try {
            ses.beginTransaction();
            n.addAll((List<Notes>) ses.createCriteria(Notes.class).list());
            Hibernate.initialize(n);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();

        }
        return n;
    }

    /**
     *
     * @param CustomerId
     * @return
     */
    public List<Notes> getByCustomer(BigInteger CustomerId) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        List<Notes> n = new ArrayList<Notes>();
        try {
            Criteria crit = ses.createCriteria(Notes.class).add(Restrictions.eq("cid", CustomerId.longValue()));
            n.addAll((List<Notes>) crit.list());
            Hibernate.initialize(n);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();

        }

        return n;

    }

    public Integer deleteNotes(BigInteger id) {
        Integer result;
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Transaction t = null;
        try {
            Notes n = (Notes) ses.get(Notes.class, id.longValue());
            if (n != null) {
                t = ses.beginTransaction();
                ses.delete(n);
                t.commit();
                result = 1;
            } else {
                result = -2;
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            t.rollback();
            result = -1;
        }
        ses.close();
        return result;
    }

    public List<NoteDetails> parseListNoteOrm(List<Notes> list) {
        List<NoteDetails> result = new ArrayList<NoteDetails>();

        try {
            for (Notes n : list) {

                result.add(this.parseObject(n));
            }

        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            result = new ArrayList();
        }

        return result;
    }

    public List<Notes> parseListNoteDetails(List<NoteDetails> list) {
        List<Notes> result = new ArrayList();
        try {
            for (NoteDetails n : list) {
                result.add(this.parseDetails(n));
            }

        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            result = new ArrayList();
        }

        return result;

    }
}
