/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iz.training.resources;

import com.iz.training.orm.Customer;
import com.iz.training.schema.CustomerDetails;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import org.springframework.stereotype.Service;

/**
 *
 * @author josechavarria
 */
@Service
public class CustomerResource implements Resource<Customer, CustomerDetails> {

    public CustomerResource() {
        HibernateUtil.getSessionFactory();
    }

    @Override
    public Customer getbyId(BigInteger cid) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Customer c;
        try {
            ses.beginTransaction();
            c = (Customer) ses.get(Customer.class, cid.longValue());
            Hibernate.initialize(c);
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.close();
            c = new Customer();
        }
        return c;
    }

    @Override
    public CustomerDetails parseObject(Customer c) {
        CustomerDetails cd = new CustomerDetails();
        try {


            cd.setCID(BigInteger.valueOf(c.getCid()));
   
            cd.setDOB(ParseUtil.ParseDate(c.getDob()));
            cd.setGENDER(c.getGender());
            cd.setNAME(c.getName());
            cd.setSURNAME(c.getSurname());
            cd.setPHONE(c.getPhone());
            cd.setNATIO(c.getNatio());
         
            return cd;
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            cd = new CustomerDetails();
        }
        return cd;
    }

    @Override
    public Customer parseDetails(CustomerDetails cd) {
        Customer c = new Customer();
        try {
            c.setCid((cd.getCID() != null ? cd.getCID().longValue() : null));
            c.setDob(ParseUtil.ParseXMLDate(cd.getDOB()));
            c.setGender(cd.getGENDER());
            c.setName(cd.getNAME());
            c.setNatio(cd.getNATIO());
            c.setPhone(cd.getPHONE());
            c.setSurname(cd.getSURNAME());
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            c = new Customer();
        }
        return c;
    }

    @Override
    public Long SaveNewObject(Customer c) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Long result;
        try {
            ses.beginTransaction();
            ses.save(c);
            ses.getTransaction().commit();
            ses.close();
            result = c.getCid();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.getTransaction().rollback();
            ses.close();
            result = (long)-1;
        }
        return result;
    }

    @Override
    public Integer UpdateObject(Customer c) {
        Session ses = HibernateUtil.getSessionFactory().openSession();
        Integer feedback = null;
        Integer result;
        try {
            Integer a = 100;
            ses.beginTransaction();
            Query query = ses.createSQLQuery("CALL UpdateCustomer(:nam,:surnam,:nati,:gend,:phon,:dob,:id);")
                    .setParameter("nam", c.getName())
                    .setParameter("surnam", c.getSurname())
                    .setParameter("nati", c.getNatio())
                    .setParameter("gend", c.getGender())
                    .setParameter("phon", c.getPhone())
                    .setParameter("dob", c.getDob())
                    .setParameter("id", c.getCid());
            result = (Integer) query.uniqueResult();
            ses.getTransaction().commit();
            ses.close();
        } catch (Exception ex) {
            Logger.getLogger(CustomerResource.class.getName()).log(Level.SEVERE, null, ex);
            ses.getTransaction().rollback();
            ses.close();
            result = -2;
        }
        return result;
    }

    @Override
    public Integer verifyObjectId(Customer c) {
         BigInteger result;
         Session ses = HibernateUtil.getSessionFactory().openSession();
        try{
        result = (BigInteger) ses.createSQLQuery("Select verifyCustomer(:CIDV)").setParameter("CIDV", c.getCid()).uniqueResult();
        }catch(Exception ex){
        
        result=BigInteger.valueOf(-1);
        }
        ses.close();
        return result.intValue();
    }

    @Override
    public List<Customer> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
