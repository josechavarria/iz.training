<%-- 
    Document   : login
    Created on : Oct 10, 2014, 10:34:20 AM
    Author     : josechavarria
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>
    <body>
        <h1>Struts2 - Spring Security Demo</h1> 
        <s:if test="%{#parameters.error != null}">
            <div style="color: red">Invalid User</div>
        </s:if>
        <s:form name="loginForm" action="j_spring_security_check" method="post">
            <s:textfield class="form-control" name="username" label="Username"/>
            <s:password class="form-control" name="password" label="Password"/>
            <s:submit class="btn btn-info" value="Login"/>
        </s:form>
    </body>
</html>